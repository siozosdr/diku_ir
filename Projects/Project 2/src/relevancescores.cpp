#include "indri/LocalQueryServer.hpp"
#include "indri/ScopedLock.hpp"
#include "indri/Index.hpp"
#include "indri/CompressedCollection.hpp"
#include "indri/Repository.hpp"
#include "indri/Collection.hpp"
#include "indri/QueryEnvironment.hpp"
#include "indri/QueryExpander.hpp"
#include "indri/TFIDFExpander.hpp"
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <sstream>
#include <string>
#include <algorithm>
#include <vector>
#include <stdlib.h>
#include <iomanip>
#include <iostream>
#include <map>
#include "indri/count_iterator"
#include <fstream>
#include <sstream>
#include <set>
#include <cmath>
#include <algorithm>
#include <cmath>

using namespace indri::api;
using namespace std;




/*
 * Compile using (where > indicate command line):
 * > make -f Makefile.app
 * 
 * If you installed Indri in the default location, Makefile.app should be found in /usr/local/share/indri/ or type: 
 * > locate Makefile.app
 * Copy Makefile.app into the same directory as this file and edit the first line, where it says:
 * APP= 
 * to 
 * APP=relevancescores
 * and then type make -f Makefile.app
 */
bool cmp(const pair<string, float>  &p1, const pair<string, float> &p2)
{
    return p1.second > p2.second;
}
int main(int argc, char ** argv){
	/*
	 * Open an index specified by indexName.
	 * Open the index as CompressedCollection where we can get the external document ids (to be used when
	 * selecting feedback documents)
	 */
	string indexName = "/home/sokras/Desktop/lemur/indexes/nytIndex/";
	indri::collection::Repository r;
	int queryCount = 51;
	r.openRead(indexName);
	indri::collection::CompressedCollection * collection = r.collection();
	indri::server::LocalQueryServer local(r);
	/*
	 * Open a QueryEnvironment. Through this we can execute queries directly through the source
	 * code into the index. Add the index to the QueryEnvironment so Indri know where to look
	 */
	indri::api::QueryEnvironment * env                   = new indri::api::QueryEnvironment();
	env->addIndex(indexName);
	map<int, vector<float> > myStuff;
	// Open query file to read
	ifstream myfile ("/home/sokras/workspace/IR-Project2/src/queries/indriq1.txt");
	string query;
	vector<int> queryLengths;
	float documentEntropy = 0;
	UINT64 docCount = local.documentCount();
	while ( getline (myfile,query) ){
		float queryLength=0;
	  cout << "Query: "<< query << '\n';
	  	/*
		 * Run the query line and get the top-10 results returned. The format of the query is always "indri".
		 * The retrieval model used is the language model with default \mu = 2500
		 */
		std::vector< indri::api::ScoredExtentResult > res = env->runQuery(query, 10, "indri");
		std::vector< indri::api::ScoredExtentResult >::iterator it;

		/*
		 * Iterate over the results and print the number the document has in the index, its external id
		 * and the score it received.
		 */
		 std::vector<lemur::api::DOCID_T> documentIDs;
		 // maps to hold tf,idf of all terms of the returned documents
		map<string, float> tfOfAllDocs;
		map<string, float> idfOfAllDocs;
		map<string, float> tfidfOfAllDocs;
		vector<pair<string, float> > tfidfOfAllDocsVector;

		//---Calculate tfidf of all document set first----------
		for(it = res.begin(); it != res.end(); ++it){
			ScoredExtentResult sr    = *it;

			std::string documentName = collection->retrieveMetadatum( sr.document, "docno" );
			//get a document from collection by ID
			indri::collection::CompressedCollection* collection = r.collection();
			indri::api::ParsedDocument* document = collection->retrieve( sr.document );
			documentIDs.push_back(sr.document);
			indri::server::QueryServerVectorsResponse* response = local.documentVectors( documentIDs );
			if( response->getResults().size() ) {
			indri::api::DocumentVector* docVector = response->getResults()[0];
			// find max size of terms array in document


			for( size_t i=0; i<docVector->positions().size(); i++ ) {
			  int position = docVector->positions()[i];
			  // retrieve stem terms
			  const std::string& stem = docVector->stems()[position];
			  // calculate tf for each doc term
			  tfOfAllDocs[stem]++;
			  //cout << "calculated tf" << endl;
			  // calculate idf for each doc term
			  double termCountInAllDocuments;
			  if(local.termCount( stem ) == 0){
				// total number of documents
				termCountInAllDocuments = docCount;
			  }else{
				termCountInAllDocuments= env->documentExpressionCount( stem );
			  }
			  //cout << "returned docCount" << endl;
			  float idf = log2(docCount / (1+abs(termCountInAllDocuments)));
			  //cout<< "calculating idf..."<< endl;
			  idfOfAllDocs[stem] = idf;

			  //cout<< "IDF of term: " << docTerm << " = " << idf << endl;
			 }

			delete docVector;
		   }

		   delete response;
		}
		// iterate through the tf map(docTerms) to calculate tfidf for each document term of the document set
		for (auto& it : tfOfAllDocs){
			//std::cout << it.first << ' ' << it.second << '\n';
			tfidfOfAllDocs[it.first] = idfOfAllDocs[it.first] * it.second;
			//cout << "Calculated tfidf of term: " << it.first << " = "<<tfidfOfAllDocs[it.first]<< endl;
		}
		// pass tfidf map values to vector and sort
		copy(tfOfAllDocs.begin(), tfOfAllDocs.end(), back_inserter(tfidfOfAllDocsVector));
		sort(tfidfOfAllDocsVector.begin(), tfidfOfAllDocsVector.end(), cmp);
		//------------------------------------------------------------------------------
		for(it = res.begin(); it != res.end(); ++it){
			ScoredExtentResult sr    = *it;

			std::string documentName = collection->retrieveMetadatum( sr.document, "docno" );
			//get a document from collection by ID
			indri::collection::CompressedCollection* collection = r.collection();
			indri::api::ParsedDocument* document = collection->retrieve( sr.document );

			// map to hold all the terms of the document, also holds the tf of the each term
			map<string, int> docTerms;
			// vector to hold all the terms of the document
			vector<string> vectorOfDocTerms;

			map<string, int> termPositions;
			// get document stems
			// map to hold the idf values of the document terms
			map<string, float> idfOfTerms;
			map<string, float> tfidfOfTerms;

			// vector to hold term-tfidf pairs
			vector<pair<string,float> > tfidfOfTermsVector;
			int docIteratorCounter = 0;
			documentIDs.push_back(sr.document);
			indri::server::QueryServerVectorsResponse* response = local.documentVectors( documentIDs );
			if( response->getResults().size() ) {
				indri::api::DocumentVector* docVector = response->getResults()[0];
				// find max size of terms array in document


				for( size_t i=0; i<docVector->positions().size(); i++ ) {
				  int position = docVector->positions()[i];
				  // retrieve stem terms
				  const std::string& stem = docVector->stems()[position];
				  /*if(stem == "[00V]"){
					  cout << "if met" << endl;
					  continue;
				  }*/
				  // calculate tf for each doc term
				  docTerms[stem]++;
				  vectorOfDocTerms.push_back(stem);
				  //cout << "calculated tf" << endl;
				  // calculate idf for each doc term
				  double termCountInAllDocuments;
				  if(local.termCount( stem ) == 0){
  					// total number of documents
					termCountInAllDocuments = docCount;
	   			  }else{
					termCountInAllDocuments= env->documentExpressionCount( stem );
				  }
				  //cout << "returned docCount" << endl;
				  float idf = log2(docCount / (1+abs(termCountInAllDocuments)));
				  //cout<< "calculating idf..."<< endl;
				  idfOfTerms[stem] = idf;

				  //cout<< "IDF of term: " << docTerm << " = " << idf << endl;
				 }

				delete docVector;
			}

			delete response;
			//----Relevance score---------
			float docRelevance = sr.score;
			//documentEntropy += calculateEntropy(document, line);
			// ---------------CALCULATE ENTROPY---------------
			std::string docTerm;
			int i =0;
			istringstream document_text(document->text);
			// divide document in 10
			// variable to find document maxsize
			int maxSize = docTerms.size();
			int threshold =maxSize/10;
			int secondaryThreshold=threshold;
			//cout << "threshold: " <<threshold << endl;
			float termEntropy = 0;
			float subsetEntropy =0;
			float tfi=0;
			float tf=0;
			float pi = 0;
			// parse query to divide into terms
			std::string queryTerm;
			std::string delimiter = " ";
			// vector for the entropy of the query
			vector<float> queryEntropy;
			std::vector<float>::iterator it;
			int vectorCounter =0;
			// map to hold the term for each subset of the document
			map<string, int> myTerms;
			istringstream query_text(query);

			while(query_text >> queryTerm){
				queryLength++;
				//cout<< "Checking term..." <<endl;
				//cout<< "Query term: " << queryTerm << endl;
				// calculate tf for query term from the document terms, based on query term
				tf = docTerms[queryTerm];

				int currentSize = 0;
				istringstream text(document->text);
				// populate map with terms of subset document
				while(document_text >> docTerm){
					if(docTerms.find(docTerm) != docTerms.end()){
						myTerms[docTerm]++;
						currentSize++;
						// calculate tf of term in document
						docTerms[docTerm] ++;
						// hold position of term in document
						if(termPositions.find(docTerm) != termPositions.end()){
							termPositions[docTerm] = currentSize;
						}
						if(currentSize == threshold){
							//cout<< "currentSize: "<<currentSize<< endl;
							// calculate pi for query term in subset i of document
							tfi = myTerms[queryTerm];
							pi = (tfi + 1)/(tf + 10);
							//cout<<"pi: "<< pi << ", tfi: " << tfi << ", tf: "<< tf<< endl;
							// empty map and move to next subset of document
							myTerms.empty();
							//cout<<"threshold before increase: "<< threshold << ", maxSize: "<<maxSize
							//		<< "docTerms Size: "<< docTerms.size()<<endl;
							threshold+=secondaryThreshold;
							// calculate term entropy and then add it to document entropy
							subsetEntropy = pi * log2(pi);
							// entropy of query term for whole document
							termEntropy += subsetEntropy;
							//cout<< "tfi: " << tfi << " ,pi: "<<pi<<" ,threshold: "<<threshold
							//		<<" ,subsetEntropy: "<<subsetEntropy << endl;

						}
					}

				}
				threshold = secondaryThreshold;
				//cout<< "termEntropy: "<<termEntropy<< endl;
				queryEntropy.insert(queryEntropy.begin()+vectorCounter,termEntropy);
				vectorCounter ++;
				termEntropy =0;
			}
			queryLengths.push_back(queryLength);
			queryLength = 0;

			float totalDocEntropy =0;
			// get mean value from terms entropy to calculate query entropy for document
			for (it=queryEntropy.begin(); it<queryEntropy.end(); it++) totalDocEntropy += *it;
			totalDocEntropy /= queryEntropy.size();

			//----------------Calculate similarity---------------
			// Source: http://www.wikiwand.com/en/Cosine_similarity

			// iterate through the tf map(docTerms) to calculate tfidf for each document term
			for (auto& it : docTerms){
				//std::cout << it.first << ' ' << it.second << '\n';
				tfidfOfTerms[it.first] = idfOfTerms[it.first] * it.second;
				//cout << "Calculated tfidf of term: " << it.first << " = "<<tfidfOfTerms[it.first]<< endl;
			}
			// pass tfidf map values to vector and sort
			copy(docTerms.begin(), docTerms.end(), back_inserter(tfidfOfTermsVector));
			sort(tfidfOfTermsVector.begin(), tfidfOfTermsVector.end(), cmp);
			// Print top 20 informative terms with their value from a single document
			int c =0;
			int c2 =0;
			float sumASquare=0;
			float sumBSquare=0;
			float sumAB=0;
			float similarity = 0;
			// calculate cosine similarity for document
			for(int i=1; i<21; i++){
				sumAB += tfidfOfTermsVector.at(i).second * tfidfOfAllDocsVector.at(i).second;
				sumASquare += pow(tfidfOfTermsVector.at(i).second,2);
				sumBSquare += pow(tfidfOfAllDocsVector.at(i).second,2);

			}

			/*cout<<"top 20 document terms:"<< endl;
			for(int i=1; i<21; i++){
				cout << tfidfOfTermsVector.at(i).first << ", "<<tfidfOfTermsVector.at(i).second<< endl;
			}
			cout<<"top 20 document set terms:"<< endl;
			for(int i=1; i<21; i++){
				cout << tfidfOfAllDocsVector.at(i).first << ", "<<tfidfOfAllDocsVector.at(i).second<< endl;
			}*/
			similarity = sumAB / (sqrt(sumASquare) * sqrt(sumBSquare));
			/*cout<<"sumAB: "<<sumAB << ", sumASquare: " << sumASquare << ", sumBSquare:" << sumBSquare << endl;
			cout<< "(sqrt(sumASquare) = " << sqrt(sumASquare) << ", sqrt(sumBSquare)= " << sqrt(sumBSquare) << endl;*/

			//--------------CALCULATE DF metric---------------------------
			/*for(int i=1;i<21;i++){
				string temp;
				temp.append(query);
				temp.append(" ");
				cout<< "appending2 done..." << endl;
				temp.append(tfidfOfTermsVector.at(i).first);
				cout<< "temp:" <<temp<< endl;
				int count = env->documentExpressionCount(temp);
				cout<< "counting done..." << endl;
				cout << "string: "<<temp <<", No. occurences: "<<count<<endl;
			}*/

			//-------------Print results-------------------
			cout << /*"Doc Nr: " << sr.document;*/queryCount/*<<", External ID: " */<< " "<<documentName;
			cout << /*", Relevance score: " <<*/ " "<<sr.score /*<< ", Entropy:"*/ <<" "<< -totalDocEntropy
				 /*<< ", Sim: "*/<<" "<<similarity <<std::endl;
		}


		queryCount++;
	}
	float averageLength=0;
	for(std::vector<int>::size_type i = 0; i != queryLengths.size(); i++) {
		averageLength+=queryLengths[i];
	}
	cout<<"average query length:" << averageLength<<endl;
	return EXIT_SUCCESS;
}
