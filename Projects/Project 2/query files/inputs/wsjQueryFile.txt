<parameters>
<index>/home/sokras/Desktop/lemur/indexes/wsjIndex</index>
<trecFormat>true</trecFormat>
<count>10</count>
<rule>method:dirichet</rule>
<query> <number>51</number> <text> #combine(Airbus Subsidies)</text></query>
<query> <number>52</number> <text> #combine(South African Sanctions)</text></query>
<query> <number>53</number> <text> #combine(Leveraged Buyouts)</text></query>
<query> <number>54</number> <text> #combine(Satellite Launch Contracts)</text></query>
<query> <number>55</number> <text> #combine(Insider Trading)</text></query>
<query> <number>56</number> <text> #combine(Prime Lending Rate Moves Predictions)</text></query>
<query> <number>57</number> <text> #combine(MCI)</text></query>
<query> <number>58</number> <text> #combine(Rail Strikes)</text></query>
<query> <number>59</number> <text> #combine(Weather Related Fatalities)</text></query>
<query> <number>60</number> <text> #combine(Merit Pay vs Seniority)</text></query>
<query> <number>61</number> <text> #combine(Israeli Role in Iran Contra Affair)</text></query>
<query> <number>62</number> <text> #combine(Military Coups Detat)</text></query>
<query> <number>63</number> <text> #combine(Machine Translation)</text></query>
<query> <number>64</number> <text> #combine(Hostage Taking)</text></query>
<query> <number>65</number> <text> #combine(Information Retrieval Systems)</text></query>
<query> <number>66</number> <text> #combine(Natural Language Processing)</text></query>
<query> <number>67</number> <text> #combine(Politically Motivated Civil Disturbances)</text></query>
<query> <number>68</number> <text> #combine(Health Hazards from Fine Diameter Fibers)</text></query>
<query> <number>69</number> <text> #combine(Attempts to Revive the SALT II Treaty)</text></query>
<query> <number>70</number> <text> #combine(Surrogate Motherhood)</text></query>
<query> <number>71</number> <text> #combine(Border Incursions)</text></query>
<query> <number>72</number> <text> #combine(Demographic Shifts in the US)</text></query>
<query> <number>73</number> <text> #combine(Demographic Shifts across National Boundaries)</text></query>
<query> <number>74</number> <text> #combine(Conflicting Policy)</text></query>
<query> <number>75</number> <text> #combine(Automation)</text></query>
<query> <number>76</number> <text> #combine(US Constitution Original Intent)</text></query>
<query> <number>77</number> <text> #combine(Poaching)</text></query>
<query> <number>78</number> <text> #combine(Greenpeace)</text></query>
<query> <number>79</number> <text> #combine(FRG Political Party Positions)</text></query>
<query> <number>80</number> <text> #combine(1988 Presidential Candidates Platforms)</text></query>
<query> <number>81</number> <text> #combine(Financial crunch for televangelists in the wake of the PTL)</text></query>
<query> <number>82</number> <text> #combine(Genetic Engineering)</text></query>
<query> <number>83</number> <text> #combine(Measures to Protect the Atmosphere)</text></query>
<query> <number>84</number> <text> #combine(Alternative renewable Energy Plant Equipment Installation)</text></query>
<query> <number>85</number> <text> #combine(Official Corruption)</text></query>
<query> <number>86</number> <text> #combine(Bank Failures)</text></query>
<query> <number>87</number> <text> #combine(Criminal Actions Against Officers of Failed)</text></query>
<query> <number>88</number> <text> #combine(Crude Oil Price Trends)</text></query>
<query> <number>89</number> <text> #combine("Downstream" Investments by OPEC Member States)</text></query>
<query> <number>90</number> <text> #combine(Data on Proven Reserves of Oil Natural Gas)</text></query>
<query> <number>91</number> <text> #combine(US Army Acquisition of Advanced Weapons Systems)</text></query>
<query> <number>92</number> <text> #combine(International Military Equipment Sales)</text></query>
<query> <number>93</number> <text> #combine(What Backing Does the National Rifle Association)</text></query>
<query> <number>94</number> <text> #combine(Computer aided Crime)</text></query>
<query> <number>95</number> <text> #combine(Computer aided Crime Detection)</text></query>
<query> <number>96</number> <text> #combine(Computer Aided Medical Diagnosis)</text></query>
<query> <number>97</number> <text> #combine(Fiber Optics Applications)</text></query>
<query> <number>98</number> <text> #combine(Fiber Optics Equipment Manufacturers)</text></query>
<query> <number>99</number> <text> #combine(Iran Contra Affair)</text></query>
<query> <number>100</number> <text> #combine(Controlling the Transfer of High Technology)</text></query>
<query> <number>101</number> <text> #combine(Design of the Star Wars Antimissile Defense System)</text></query>
<query> <number>102</number> <text> #combine(Laser Research Applicable to the USs Strategic Defense)</text></query>
<query> <number>103</number> <text> #combine(Welfare Reform)</text></query>
<query> <number>104</number> <text> #combine(Catastrophic Health Insurance)</text></query>
<query> <number>105</number> <text> #combine(Black Monday)</text></query>
<query> <number>106</number> <text> #combine(US Control of Insider Trading)</text></query>
<query> <number>107</number> <text> #combine(Japanese Regulation of Insider Trading)</text></query>
<query> <number>108</number> <text> #combine(Japanese Protectionist Measures)</text></query>
<query> <number>109</number> <text> #combine(Find Innovative Companies)</text></query>
<query> <number>110</number> <text> #combine(Black Resistance Against the South African Government)</text></query>
<query> <number>111</number> <text> #combine(Nuclear Proliferation)</text></query>
<query> <number>112</number> <text> #combine(Funding Biotechnology)</text></query>
<query> <number>113</number> <text> #combine(New Space Satellite Applications)</text></query>
<query> <number>114</number> <text> #combine(Non commercial Satellite Launches)</text></query>
<query> <number>115</number> <text> #combine(Impact of the 1986 Immigration Law)</text></query>
<query> <number>116</number> <text> #combine(Generic Drug Substitutions)</text></query>
<query> <number>117</number> <text> #combine(Capacity of the S Cellular Telephone Network)</text></query>
<query> <number>118</number> <text> #combine(International Terrorists)</text></query>
<query> <number>119</number> <text> #combine(Actions Against International Terrorists)</text></query>
<query> <number>120</number> <text> #combine(Economic Impact of International Terrorism)</text></query>
<query> <number>121</number> <text> #combine(Death from Cancer)</text></query>
<query> <number>122</number> <text> #combine(RDT and E of New Cancer Fighting Drugs)</text></query>
<query> <number>123</number> <text> #combine(Research into and Control of Carcinogens)</text></query>
<query> <number>124</number> <text> #combine(Alternatives to Traditional Cancer Therapies)</text></query>
<query> <number>125</number> <text> #combine(Anti smoking Actions by Government)</text></query>
<query> <number>126</number> <text> #combine(Medical Ethics and Modern Technology)</text></query>
<query> <number>127</number> <text> #combine(US USSR Arms Control Agreements)</text></query>
<query> <number>128</number> <text> #combine(Privatization of State Assets)</text></query>
<query> <number>129</number> <text> #combine(Soviet Spying on the US)</text></query>
<query> <number>130</number> <text> #combine(Jewish Emigration and US USSR Relations)</text></query>
<query> <number>131</number> <text> #combine(McDonnell Douglas Contracts for Military Aircraft)</text></query>
<query> <number>132</number> <text> #combine(Stealth Aircraft)</text></query>
<query> <number>133</number> <text> #combine(Hubble Space Telescope)</text></query>
<query> <number>134</number> <text> #combine(The Human Genome Project)</text></query>
<query> <number>135</number> <text> #combine(Possible Contributions of Gene Mapping to Medicine)</text></query>
<query> <number>136</number> <text> #combine(Diversification by Pacific Telesis)</text></query>
<query> <number>137</number> <text> #combine(Expansion in the US Theme Park Industry)</text></query>
<query> <number>138</number> <text> #combine(Iranian Support for Lebanese Hostage takers)</text></query>
<query> <number>139</number> <text> #combine(Irans Islamic Revolution Domestic and Foreign Social)</text></query>
<query> <number>140</number> <text> #combine(Political Impact of Islamic Fundamentalism)</text></query>
<query> <number>141</number> <text> #combine(Japans Handling of its Trade Surplus with the US)</text></query>
<query> <number>142</number> <text> #combine(Impact of Government Regulated Grain Farming on International)</text></query>
<query> <number>143</number> <text> #combine(Why Protect US Farmers)</text></query>
<query> <number>144</number> <text> #combine(Management Problems at the United Nations)</text></query>
<query> <number>145</number> <text> #combine(Influence of the Pro Israel Lobby)</text></query>
<query> <number>146</number> <text> #combine(Negotiating an End to the Nicaraguan Civil War)</text></query>
<query> <number>147</number> <text> #combine(Productivity Trends in the US Economy)</text></query>
<query> <number>148</number> <text> #combine(Conflict in the Horn of Africa)</text></query>
<query> <number>149</number> <text> #combine(Industrial Espionage)</text></query>
<query> <number>150</number> <text> #combine(US Political Campaign Financing)</text></query>
<query> <number>151</number> <text> #combine(Coping with overcrowded prisons)</text></query>
<query> <number>152</number> <text> #combine(Accusations of Cheating by Contractors on US Defense Projects)</text></query>
<query> <number>153</number> <text> #combine(Insurance Coverage which pays for Long Term Care)</text></query>
<query> <number>154</number> <text> #combine(Oil Spills)</text></query>
<query> <number>155</number> <text> #combine(Right Wing Christian Fundamentalism in US)</text></query>
<query> <number>156</number> <text> #combine(Efforts to enact Gun Control Legislation)</text></query>
<query> <number>157</number> <text> #combine(Causes and treatments of multiple sclerosis MS)</text></query>
<query> <number>158</number> <text> #combine(Term limitations for members of the US Congress)</text></query>
<query> <number>159</number> <text> #combine(Electric Car Development)</text></query>
<query> <number>160</number> <text> #combine(Vitamins The Cure for or Cause of)</text></query>
<query> <number>161</number> <text> #combine(Acid Rain)</text></query>
<query> <number>162</number> <text> #combine(Automobile Recalls)</text></query>
<query> <number>163</number> <text> #combine(Vietnam Veterans and Agent Orange)</text></query>
<query> <number>164</number> <text> #combine(Generic Drugs Illegal Activities by Manufacturers)</text></query>
<query> <number>165</number> <text> #combine(Tobacco company advertising and the young)</text></query>
<query> <number>166</number> <text> #combine(Standardized testing and cultural bias)</text></query>
<query> <number>167</number> <text> #combine(Regulation of the showing of violence and explicit)</text></query>
<query> <number>168</number> <text> #combine(Financing AMTRAK)</text></query>
<query> <number>169</number> <text> #combine(Cost of Garbage Trash Removal)</text></query>
<query> <number>170</number> <text> #combine(The Consequences of Implantation of Silicone Gel)</text></query>
<query> <number>171</number> <text> #combine(Use of Mutual Funds in an Individuals)</text></query>
<query> <number>172</number> <text> #combine(The Effectiveness of Medical Products and)</text></query>
<query> <number>173</number> <text> #combine(Smoking Bans)</text></query>
<query> <number>174</number> <text> #combine(Hazardous Waste Cleanup)</text></query>
<query> <number>175</number> <text> #combine(NRA Prevention of Gun Control Legislation)</text></query>
<query> <number>176</number> <text> #combine(Real life private investigators)</text></query>
<query> <number>177</number> <text> #combine(English as the Official Language in US)</text></query>
<query> <number>178</number> <text> #combine(Dog Maulings)</text></query>
<query> <number>179</number> <text> #combine(US Restaurants in Foreign Lands)</text></query>
<query> <number>180</number> <text> #combine(Ineffectiveness of US Embargoes Sanctions)</text></query>
<query> <number>181</number> <text> #combine(Abuse of the Elderly by Family Members and Medical and)</text></query>
<query> <number>182</number> <text> #combine(Commercial Overfishing Creates Food Fish Deficit)</text></query>
<query> <number>183</number> <text> #combine(Asbestos Related Lawsuits)</text></query>
<query> <number>184</number> <text> #combine(Corporate Pension Plans Funds)</text></query>
<query> <number>185</number> <text> #combine(Reform of the US Welfare System)</text></query>
<query> <number>186</number> <text> #combine(Difference of Learning Levels Among Inner)</text></query>
<query> <number>187</number> <text> #combine(Signs of the Demise of Independent Publishing)</text></query>
<query> <number>188</number> <text> #combine(Beachfront Erosion)</text></query>
<query> <number>189</number> <text> #combine(Real Motives for Murder)</text></query>
<query> <number>190</number> <text> #combine(Instances of Fraud Involving the Use of a Computer)</text></query>
<query> <number>191</number> <text> #combine(Efforts to Improve US Schooling)</text></query>
<query> <number>192</number> <text> #combine(Oil Spill Cleanup)</text></query>
<query> <number>193</number> <text> #combine(Toys R Dangerous)</text></query>
<query> <number>194</number> <text> #combine(The Amount of Money Earned by Writers)</text></query>
<query> <number>195</number> <text> #combine(Stock Market Perturbations Attributable to)</text></query>
<query> <number>196</number> <text> #combine(School Choice Voucher System and its effects)</text></query>
<query> <number>197</number> <text> #combine(Reform of the jurisprudence system)</text></query>
<query> <number>198</number> <text> #combine(Gene Therapy and Its Benefits to)</text></query>
<query> <number>199</number> <text> #combine(Legality of Medically Assisted Suicides)</text></query>
<query> <number>200</number> <text> #combine(Impact of foreign textile imports on US)</text></query>
<query> <number>201</number> <text> #combine(What procedures should be implemented to insure that proper care is given to children placed under the au pairs responsibility)</text></query>
<query> <number>202</number> <text> #combine(Status of nuclear proliferation treaties violations and monitoring)</text></query>
<query> <number>203</number> <text> #combine(What is the economic impact of recycling tires)</text></query>
<query> <number>204</number> <text> #combine(Where are the nuclear power plants in the US and what has been their rate of production)</text></query>
<query> <number>205</number> <text> #combine(What evidence is there of paramilitary activity in the US)</text></query>
<query> <number>206</number> <text> #combine(Prognosis viability of a political third party in US)</text></query>
<query> <number>207</number> <text> #combine(What are the prospects of the Quebec separatists achieving independence from the rest of Canada)</text></query>
<query> <number>208</number> <text> #combine(What are the latest developments in bioconversion the conversion of biological waste garbage and plant material into energy fertilizer and other useful products)</text></query>
<query> <number>209</number> <text> #combine(Identify what is being done or what ideas are being proposed to ensure that Social Security will not go broke)</text></query>
<query> <number>210</number> <text> #combine(How widespread is the illegal disposal of medical waste in the US and what is being done to combat this dumping)</text></query>
<query> <number>211</number> <text> #combine(How effective are the driving while intoxicated DWI regulations Has the number of deaths caused by DWI been significantly lowered Why arent penalties as harsh for DWI drivers as for the sober driver)</text></query>
<query> <number>212</number> <text> #combine(What countries have been accused of failing to adequately protect US copyrights patents and trademarks This is considered a violation of ntellectual property laws)</text></query>
<query> <number>213</number> <text> #combine(As a result of DNA testing are more defendants being absolved or convicted of crimes)</text></query>
<query> <number>214</number> <text> #combine(What are the different techniques used to create self  induced hypnosis)</text></query>
<query> <number>215</number> <text> #combine(Why is the infant mortality rate in the United States higher than it is in most other industrialized nations)</text></query>
<query> <number>216</number> <text> #combine(What research is ongoing to reduce the effects of osteoporosis in existing patients as well as prevent the disease occurring in those unafflicted at this time)</text></query>
<query> <number>217</number> <text> #combine(Reporting on possibility of and search for extra  terrestrial life intelligence)</text></query>
<query> <number>218</number> <text> #combine(How have mini steel mills changed the steel industry)</text></query>
<query> <number>219</number> <text> #combine(How has the volume of US imports of Japanese autos compared with export of US autos to Canada and Mexico)</text></query>
<query> <number>220</number> <text> #combine(How do crossword puzzle makers go about making their puzzles)</text></query>
<query> <number>221</number> <text> #combine(Steps taken by church governments community civic organizations to halt carnage among youths engaged in drug or gang warfare)</text></query>
<query> <number>222</number> <text> #combine(Is there data available to suggest that capital punishment is a deterrent to crime)</text></query>
<query> <number>223</number> <text> #combine(What was responsible for the great emergence of MICROSOFT in the computer industry)</text></query>
<query> <number>224</number> <text> #combine(What can be done to lower blood pressure for people diagnosed with high blood pressure Include benefits and side effects)</text></query>
<query> <number>225</number> <text> #combine(What is the main function of the Federal Emergency Management Agency FEMA and the funding level provided to meet emergencies Also what resources are available to FEMA such as people equipment facilities)</text></query>
<query> <number>226</number> <text> #combine(Have large scale state allowed lotteries gambling improved the states financial conditions Any reduction noted in property tax state income tax roads)</text></query>
<query> <number>227</number> <text> #combine(Identify instances and reasons of deaths in the US military caused by other than enemy eg friendly fire training accidents)</text></query>
<query> <number>228</number> <text> #combine(What are some of the biggest success stories in recent years concerning environmental recovery from pollution)</text></query>
<query> <number>229</number> <text> #combine(What is being done to help people suffering from schizophrenia)</text></query>
<query> <number>230</number> <text> #combine(Is the automobile industry making an honest effort to develop and produce an electric powered automobile)</text></query>
<query> <number>231</number> <text> #combine(Should the US Government provide increased support to the National Endowment for the Arts)</text></query>
<query> <number>232</number> <text> #combine(Reports of and evaluation on the near death experience)</text></query>
<query> <number>233</number> <text> #combine(What are the latest developments of coal generation for electrical power)</text></query>
<query> <number>234</number> <text> #combine(What progress has been made in fuel cell technology)</text></query>
<query> <number>235</number> <text> #combine(What support is there in the US for legalizing drugs)</text></query>
<query> <number>236</number> <text> #combine(Are current laws of the sea uniform If not what are some of the areas of disagreement)</text></query>
<query> <number>237</number> <text> #combine(Identify alternative sources of energy for automobiles Include additives to gasoline that either decrease pollution or reduce oil consumption)</text></query>
<query> <number>238</number> <text> #combine(What is the function annual budget and or cost involved with the management and upkeep of the US National Parks)</text></query>
<query> <number>239</number> <text> #combine(Are there certain regions in the United States where specific cancers seem to be concentrated What conditions exist that might cause this problem)</text></query>
<query> <number>240</number> <text> #combine(What controls agreements technological advances or equipment are now in use or planned to assist in combating terrorism)</text></query>
<query> <number>241</number> <text> #combine(Find examples of doctor and lawyer groups considering penalties against members of their professions for malfeasance and show the results of such investigations)</text></query>
<query> <number>242</number> <text> #combine(How has affirmative action affected the construction industry)</text></query>
<query> <number>243</number> <text> #combine(Should the government restrict the use of fossil fuels as energy resources by private corporations and public utilities)</text></query>
<query> <number>244</number> <text> #combine(Status of trade balance with Japan deficit problem)</text></query>
<query> <number>245</number> <text> #combine(What are the trends and developments in retirement communities)</text></query>
<query> <number>246</number> <text> #combine(What is the extent of US arms exports)</text></query>
<query> <number>247</number> <text> #combine(What are the predictions concerning the economic effects of the UK Continent underwater tunnel Chunnel Will it present more problems han advantages for the British)</text></query>
<query> <number>248</number> <text> #combine(What are some developments in electronic technology being applied to and resulting in advances for the blind)</text></query>
<query> <number>249</number> <text> #combine(How has the depletion or destruction of the rain forest effected the worlds weather)</text></query>
<query> <number>250</number> <text> #combine(Does available data show a positive correlation between the sales of firearms and ammunition in the US and the commission of crimes involving firearms)</text></query>
<query> <number>251</number> <text> #combine(Exportation of Industry)</text></query>
<query> <number>252</number> <text> #combine(Combating Alien Smuggling)</text></query>
<query> <number>253</number> <text> #combine(Cryonic suspension services)</text></query>
<query> <number>254</number> <text> #combine(Non invasive procedures for persons with heart ailments)</text></query>
<query> <number>255</number> <text> #combine(Environmental Protection)</text></query>
<query> <number>256</number> <text> #combine(Negative Reactions to Reduced Requirements)</text></query>
<query> <number>257</number> <text> #combine(Cigarette Consumption)</text></query>
<query> <number>258</number> <text> #combine(Computer Security)</text></query>
<query> <number>259</number> <text> #combine(New Kennedy Assassination Theories)</text></query>
<query> <number>260</number> <text> #combine(Evidence of human life)</text></query>
<query> <number>261</number> <text> #combine(Threat posed by Fissionable Material)</text></query>
<query> <number>262</number> <text> #combine(Seasonal affective disorder syndrome SADS)</text></query>
<query> <number>263</number> <text> #combine(Algae as Food Supplement)</text></query>
<query> <number>264</number> <text> #combine(US Citizens in Foreign Jails)</text></query>
<query> <number>265</number> <text> #combine(Domestic Violence	)</text></query>
<query> <number>266</number> <text> #combine(Professional Scuba Diving)</text></query>
<query> <number>267</number> <text> #combine(Firefighter Training)</text></query>
<query> <number>268</number> <text> #combine(Cost of national defense)</text></query>
<query> <number>269</number> <text> #combine(Foreign Trade)</text></query>
<query> <number>270</number> <text> #combine(Control of Food Supplements)</text></query>
<query> <number>271</number> <text> #combine(Solar Power)</text></query>
<query> <number>272</number> <text> #combine(Outpatient Surgery)</text></query>
<query> <number>273</number> <text> #combine(Volcanic and Seismic activity levels)</text></query>
<query> <number>274</number> <text> #combine(Electric Automobiles)</text></query>
<query> <number>275</number> <text> #combine(Herbal Food SupplementsNatural Health)</text></query>
<query> <number>276</number> <text> #combine(Imposition of a school uniform or dress code)</text></query>
<query> <number>277</number> <text> #combine(Civilian Deaths from Land Mines)</text></query>
<query> <number>278</number> <text> #combine(DNA Information about Human Ancestry)</text></query>
<query> <number>279</number> <text> #combine(Earth magnetic pole shifting)</text></query>
<query> <number>280</number> <text> #combine(Ban on Ivory Trade)</text></query>
<query> <number>281</number> <text> #combine(Genetic code of yeast)</text></query>
<query> <number>282</number> <text> #combine(Violent Juvenile Crime)</text></query>
<query> <number>283</number> <text> #combine(China Trade)</text></query>
<query> <number>284</number> <text> #combine(International Drug Enforcement Cooperation)</text></query>
<query> <number>285</number> <text> #combine(World submarine forces)</text></query>
<query> <number>286</number> <text> #combine(Paper Cost)</text></query>
<query> <number>287</number> <text> #combine(Electronic Surveillance)</text></query>
<query> <number>288</number> <text> #combine(Weight Control Diets)</text></query>
<query> <number>289</number> <text> #combine(For profit hospitals)</text></query>
<query> <number>290</number> <text> #combine(Foreign automobile manufacturers in US)</text></query>
<query> <number>291</number> <text> #combine(Source of taxes)</text></query>
<query> <number>292</number> <text> #combine(Worldwide Welfare)</text></query>
<query> <number>293</number> <text> #combine(Evacuation of US Citizens by US Military)</text></query>
<query> <number>294</number> <text> #combine(Animal husbandry for exotic animals)</text></query>
<query> <number>295</number> <text> #combine(Deaths from Scuba Diving)</text></query>
<query> <number>296</number> <text> #combine(Trash TV)</text></query>
<query> <number>297</number> <text> #combine(Right To Die Pros and Cons)</text></query>
<query> <number>298</number> <text> #combine(Gun Control)</text></query>
<query> <number>299</number> <text> #combine(Impact on local economies of military downsizing)</text></query>
<query> <number>300</number> <text> #combine(Air Traffic Control Systems)</text></query>
</parameters>