import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.Date;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class IR_Project {
	 // source to read a file from relative path
	 // http://stackoverflow.com/questions/19874066/how-to-read-text-file-relative-path
	static String filePath = new File("").getAbsolutePath();
    static int i=0;
    static int anonID;
    static String query;
    static DateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	     
    static Date queryTime = new Date();
    static int itemRank=-1;
    static String clickURL=null;
    
    
	 public static void main(String Args[]) throws ParseException, IOException{
	     /*int i=countSessions();
	     System.out.println("sessions:"+i);
	     int req = nonEmptyRequests();
	     System.out.println("non-empty:"+req);
		 System.out.println("non-empty queries: "+ nonEmptyQueries());
		 System.out.println("Exact-same-as-before: " + exactSameAsBefore());
		 queryVolume();
		 mostCommonWords();
		HashMap<Integer, Integer> result = numberOfTerms();
		 for(int i=0;i<6;i++){
			 System.out.println("Queries:"+result.get(i));
		 }
		 System.out.println(maxQueryTerms());
		 HashMap<Integer, Integer> result = operatorsInQueries();
		 for(int i=0;i<6;i++){
			 System.out.println("Operators:"+result.get(i));
		 }
		 System.out.println("Max: "+ maxOperators());
		 float op = totalOperators();
		 System.out.println("Total operators:" + op);
		 System.out.println("Average operators: "+ 22425657/op);
		 HashMap<Integer, Integer> result = queriesPerSession();
		 for(int i=1;i<=4;i++){
			 System.out.println(i +" queries per session:"+result.get(i));
		 }
		 //System.out.println(maxQueriesPerSession());
		 ArrayList<IR_Tuple> result = mostCommonNonStopWords();
		 for(int i=0; i< 50 ; i++){
			 System.out.println(result.get(i).getTerm() + " & "+result.get(i).getValue() + "\\");
		 }*/
		 //System.out.println(percentageOfQueriesAskedByOneUsers());
		 //System.out.println(deletedTermsInQueries());
		 //System.out.println(addedTermsInQueries());
		 //System.out.println(totalChangeInQueries());
		 //mostPopularQueries();
		 //distrinctQueriesAskedFrequency();
		 HashMap<String, Integer> commonTerms = mostCommon5000Terms();
		 HashMap<IR_Record, Boolean> newLog = filteredLog(commonTerms);
		 System.out.println(newLog.size());
		 
	 }
	 
	 public static int countSessions() throws ParseException, IOException{
		 String line = null;
		 int sessions=0;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
		 int previousID = 0;
		 Date previousTime = null;
		 boolean firstLine = true;
		 try
	        {       
			 reader = new BufferedReader(new FileReader(filePath 
			   		 + "/src/DBTextFiles/user-ct-test-collection-05.txt"));
			/*reader = new BufferedReader(new FileReader(filePath 
			   		 + "/src/DBTextFiles/ses.txt"));*/
	               
	            
	            while ((line = reader.readLine()) != null)
	            {
	            	//tokenize first line
	            	if(firstLine==true){
	            		st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		                
		             	previousID = Integer.parseInt(tokenized.get(0));
		             	previousTime = DateFormat.parse(tokenized.get(2));
		             	firstLine=false;
		             	tokenized.clear();
		             	continue;
	            	}
	            	st = new StringTokenizer(line, "\t");
	                while(st.hasMoreTokens()){
	                	tokenized.add(st.nextToken());
	                }
	                
	             	anonID = Integer.parseInt(tokenized.get(0));
	             	query = tokenized.get(1);
	             	queryTime = DateFormat.parse(tokenized.get(2));
	             	
	             	//difference in seconds
	             	long dateDiff = queryTime.getTime()-previousTime.getTime();
	             	long diffSeconds = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
	             	//System.out.println("Difference:"+diffSeconds);
	             	//System.out.println("ID Difference:" + (anonID!=previousID));
                    //System.out.println("PREVIOUS: "+previousID+" "+previousQuery+ " "+previousTime+" "+previousRank+" "+clickURL);
	             	if(anonID!=previousID || (diffSeconds >= 300 && anonID == previousID)){
	             		sessions++;
	             	}
	             	//set current variables to previous, to prepare for next line
	             	previousID=anonID;
	             	previousTime=queryTime;
                    //System.out.println(anonID+" "+query+ " "+queryTime+" "+itemRank+" "+clickURL);
                    tokenized.clear();
	            }
	        }
	        catch (IOException ex)
	        {
	            ex.printStackTrace();
	        }               

	        finally
	        {
	            reader.close();
	        }
		 return sessions;
	 }

	 public static int nonEmptyRequests() throws ParseException, IOException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
		 int nonEmpty=0;
		 try
	        {       
			 reader = new BufferedReader(new FileReader(filePath 
			   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
			/*reader = new BufferedReader(new FileReader(filePath 
			   		 + "/src/DBTextFiles/ses.txt"));*/
	               
	            
	            while ((line = reader.readLine()) != null)
	            {
	            	
	            	st = new StringTokenizer(line, "\t");
	                while(st.hasMoreTokens()){
	                	tokenized.add(st.nextToken());
	                }
	                
	             	anonID = Integer.parseInt(tokenized.get(0));
	             	query = tokenized.get(1);
	             	//System.out.println("Query: "+query);
	             	if(!query.equals("-")){
	             		nonEmpty++;
	             	}
	             	tokenized.clear();
	            }
	        }
	        catch (IOException ex)
	        {
	            ex.printStackTrace();
	        }               

	        finally
	        {
	            reader.close();
	        }
		 return nonEmpty;
	 }
		  
	 public static int nonEmptyQueries() throws ParseException, IOException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
		 int previousID = 0;
		 String previousQuery = null;
		 Date previousTime = null;
		 boolean firstLine = true;
		 int nonEmptyQ = 0;
		try
	        {       
			 reader = new BufferedReader(new FileReader(filePath 
			   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
			/*reader = new BufferedReader(new FileReader(filePath 
			   		 + "/src/DBTextFiles/ses.txt"));*/
	               
	            
	            while ((line = reader.readLine()) != null)
	            {
	            	//tokenize first line
	            	if(firstLine==true){
	            		st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		                
		             	previousID = Integer.parseInt(tokenized.get(0));
		             	previousQuery = tokenized.get(1);
		             	previousTime = DateFormat.parse(tokenized.get(2));
		             	firstLine=false;
		             	tokenized.clear();
		             	continue;
	            	}
	            	st = new StringTokenizer(line, "\t");
	                while(st.hasMoreTokens()){
	                	tokenized.add(st.nextToken());
	                }
	                
	             	anonID = Integer.parseInt(tokenized.get(0));
	             	query = tokenized.get(1);
	             	queryTime = DateFormat.parse(tokenized.get(2));
	             	if(tokenized.size() >3){
	             		//itemRank = Integer.parseInt(tokenized.get(3));
	             		clickURL = tokenized.get(4);
	             	}
	             	//difference in seconds
	             	long dateDiff = queryTime.getTime()-previousTime.getTime();
	             	long diffSeconds = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
	             	
	             	// if it's a new user then new query
	             	
	             	if (    (anonID != previousID) 
	             	     && (!query.equals("-"))
	             	   || 
	             	    // if it's the same user but the query is different than the
		             	// previous one then it's a new query 		
	             	   (    (anonID == previousID)
	             	     && (!query.equals("-"))
	             	     && (!query.equals(previousQuery))
	             	   )
	             	   ||
	             	   // if it's the same user with the same query but with a time
	             	   // difference more than 300 seconds 
	             	   // then it's a new query
	             	   ( 	(anonID == previousID)
	  	             	     && (!query.equals("-"))
		             	     && (query.equals(previousQuery))
		             	     && (diffSeconds > 300))
	             	  ){
	             		nonEmptyQ ++;
	             	}
	             	
	             	//set current variables to previous, to prepare for next line
	             	previousID=anonID;
	             	previousQuery=query;
	             	previousTime=queryTime;
                    tokenized.clear();
	            }
	        }
	        catch (IOException ex)
	        {
	            ex.printStackTrace();
	        }               

	        finally
	        {
	            reader.close();
	        }
		 return nonEmptyQ;
	 }
	 
	 public static int exactSameAsBefore() throws ParseException, IOException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
		 String previousQuery = null;
		 boolean firstLine = true;
		 int exactSameAsBefore=0;
		try
	        {       
			 reader = new BufferedReader(new FileReader(filePath 
			   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
			/*reader = new BufferedReader(new FileReader(filePath
			   		 + "/src/DBTextFiles/ses.txt"));*/
	               
	            
	            while ((line = reader.readLine()) != null)
	            {
	            	//tokenize first line
	            	if(firstLine==true){
	            		st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		                
		             	previousQuery = tokenized.get(1);
		             	firstLine=false;
		             	tokenized.clear();
		             	continue;
	            	}
	            	st = new StringTokenizer(line, "\t");
	                while(st.hasMoreTokens()){
	                	tokenized.add(st.nextToken());
	                }
	                
	             	anonID = Integer.parseInt(tokenized.get(0));
	             	query = tokenized.get(1);
	             	queryTime = DateFormat.parse(tokenized.get(2));
	             	
	             	if(query.equals(previousQuery)
	             	   && !query.equals("-")
	             	   && !previousQuery.equals("-")){
	             		exactSameAsBefore++;
	             	}
	             	
	             	//set current variables to previous, to prepare for next line
	             	previousQuery=query;
                    tokenized.clear();
	            }
	            
	        }
	        catch (IOException ex)
	        {
	            ex.printStackTrace();
	        }               

	        finally
	        {
	            reader.close();
	        }
		 return exactSameAsBefore;
	 }
	 
	 @SuppressWarnings("deprecation")
	 public static int[] queryVolume() throws IOException, ParseException{
			 int result[] = new int[24];
			 String line = null;
			 BufferedReader reader = null;
	         ArrayList<String> tokenized = new ArrayList<String>();
	         StringTokenizer st;
	         for(int j=1;j<=10;j++){
	        	 String identifier = String.valueOf(j);
				try
			        {       
					if(j<10){
						reader = new BufferedReader(new FileReader(filePath 
						   		 + "/src/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
						/*reader = new BufferedReader(new FileReader(filePath 
						   		 + "/src/DBTextFiles/ses.txt"));*/
					}
					else{
						reader = new BufferedReader(new FileReader(filePath 
						   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
					}
					 
			               
			            
			            while ((line = reader.readLine()) != null)
			            {
			            	st = new StringTokenizer(line, "\t");
			                while(st.hasMoreTokens()){
			                	tokenized.add(st.nextToken());
			                }
			                
			             	anonID = Integer.parseInt(tokenized.get(0));
			             	query = tokenized.get(1);
			             	queryTime = DateFormat.parse(tokenized.get(2));
			             	if(tokenized.size() >3){
			             		//itemRank = Integer.parseInt(tokenized.get(3));
			             		clickURL = tokenized.get(4);
			             	}
			             
			             	/*
			             	 * Check the hour with queryTime.getHours() in each query time. 
			             	 * depending on the HH I insert it to a hashmap or a table and 
			             	 * then i plot the data based on the table.
			             	 * */
			             	result[queryTime.getHours()]++;
			             	tokenized.clear();
			            }
			        }
			        catch (IOException ex)
			        {
			            ex.printStackTrace();
			        }               
		
			        finally
			        {
			            reader.close();
			        }
				System.out.println("Progress:" + j+"0%");
	         }
			 return result;
		 }
	 
	 public static ArrayList<IR_Tuple> mostCommonWords() throws IOException, ParseException{
		 HashMap<String, Integer> words = new HashMap<>();
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         StringTokenizer qt;
         // stopwords
         HashMap<String, Integer> stopWords = new HashMap<>();
         reader = new BufferedReader(new FileReader(filePath 
		   		 + "/src/stopwords.txt"));
         while((line = reader.readLine()) != null){
        	 st = new StringTokenizer(line);
        	 String w = st.nextToken();
        	 stopWords.put(w,1);
         }
         
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	qt = new StringTokenizer(query, " ");
		             	// populate hashmap
		             	while(qt.hasMoreTokens()){
		             		String token = qt.nextToken();
		             		int v;		             			
		             		if(words.containsKey(token)){
		             			v = words.get(token);
		             			// if word doesnt exist in stopwords list
		             			if(stopWords.get(token) == null){
			             			words.put(token, v+1);
		             			}
		             		}else{
		             			// if word doesnt exist in stopwords list	
		             			if(stopWords.get(token) == null){
		             				v = 1;
			             			words.put(token, v);
		             			}
		             			
		             		}
		             	}
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }
         // Sort hashmap
         ArrayList<IR_Tuple>sorted = new ArrayList<IR_Tuple>();
         //iterate through map and put key-values to the arraylist
		for (Entry<String, Integer> entry : words.entrySet())
		{
		    //System.out.println(entry.getKey() + "/" + entry.getValue());
		    IR_Tuple a = new IR_Tuple(entry.getKey(), entry.getValue());
		    sorted.add(a);
		}
		 //sort arraylist
		Comparator<IR_Tuple> comparator = new Comparator<IR_Tuple>(){

					@Override
					public int compare(IR_Tuple o1, IR_Tuple o2) {
						// TODO Auto-generated method stub
						if(o1.getValue() < o2.getValue()){
							return 1;
						}
						else if(o1.getValue() > o2.getValue()){
							return -1;
						}
						return 0;
					}

		};
		Collections.sort(sorted,comparator);


        
         //return top 200 to result.
         
         for(int i=0 ; i< 200; ++i) {
             System.out.println(sorted.get(i).getTerm() );
         }
         
         
         System.out.println("end");
		 return sorted;
	 }
	 
	 public static HashMap<Integer,Integer> numberOfTerms() throws IOException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         StringTokenizer qt;
         HashMap<Integer, Integer> result = new HashMap<Integer,Integer>();
         ArrayList<String> tokenizedQuery = new ArrayList<String>();         
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	// 0 terms
		             	if(query.equals("-")){
		             		if(result.get(0)!= null){
		             			int v = result.get(0);
			             		result.put(0, v+1);
		             		}else{
		             			result.put(0,1);
		             		}
		             	}
		             	qt = new StringTokenizer(query, " ");
		             	while(qt.hasMoreTokens()){
		             		String term = qt.nextToken();
		             		tokenizedQuery.add(term);
		             	}
		             	// 1 term
		             	if(tokenizedQuery.size() == 1 && tokenizedQuery.get(0)!= "-"){
		             		if(result.get(1)!= null){
		             			int v = result.get(1);
			             		result.put(1, v+1);
		             		}else{
		             			result.put(1,1);
		             		}
		             	// 2 terms
		             	}else if(tokenizedQuery.size() == 2){
		             		if(result.get(2)!= null){
		             			int v = result.get(2);
			             		result.put(2, v+1);
		             		}else{
		             			result.put(2,1);
		             		}
		             	// 3 terms
		             	}else if(tokenizedQuery.size() == 3){
		             		if(result.get(3)!= null){
		             			int v = result.get(3);
			             		result.put(3, v+1);
		             		}else{
		             			result.put(3,1);
		             		}
		             	// more than 3 terms
		             	}else{
		             		if(result.get(4)!= null){
		             			int v = result.get(4);
			             		result.put(4, v+1);
		             		}else{
		             			result.put(4,1);
		             		}
		             	}
		             	tokenizedQuery.clear();
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }         
         
         System.out.println("end");
		 return result; 
	 }

	 public static int maxQueryTerms() throws IOException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         StringTokenizer qt;
         int result = 0;
         ArrayList<String> tokenizedQuery = new ArrayList<String>();         
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	qt = new StringTokenizer(query, " ");
		             	while(qt.hasMoreTokens()){
		             		String term = qt.nextToken();
		             		tokenizedQuery.add(term);
		             	}
		             	if(tokenizedQuery.size()>result){
		             		result = tokenizedQuery.size();
		             	}
		             	tokenizedQuery.clear();
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }         
         
         System.out.println("end");
		 return result; 
	 }

	 public static HashMap<Integer,Integer> operatorsInQueries() throws IOException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         HashMap<Integer, Integer> result = new HashMap<Integer,Integer>();
         ArrayList<String> tokenizedQuery = new ArrayList<String>();         
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	
		            	
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	if(query.equals("-")){
		             		tokenized.clear();
		             		System.out.println("skipped");
		             		continue;
		             	}
		             	int count = 0;
		                Pattern p = Pattern.compile("and");
		                Matcher m = p.matcher(query);
		                while (m.find()){
		                	count +=1;
		                }
		                p = Pattern.compile("or");
		                m = p.matcher(query);
		                while (m.find()){
		                	count +=1;
		                }
		                p = Pattern.compile("not");
		                m = p.matcher(query);
		                while (m.find()){
		                	count +=1;
		                }
		                p = Pattern.compile("near");
		                m = p.matcher(query);
		                while (m.find()){
		                	count +=1;
		                }
		                
		            	count += query.length() - query.replace("-", "").length();
		            	count += query.length() - query.replace("+", "").length();
		            	

		            	// 0 terms
		             	if(count == 0){
		             		if(result.get(0)!= null){
		             			int v = result.get(0);
			             		result.put(0, v+1);
		             		}else{
		             			result.put(0,1);
		             		}
		             	}
		            	// 1 term
		             	if(count == 1){
		             		if(result.get(1)!= null){
		             			int v = result.get(1);
			             		result.put(1, v+1);
		             		}else{
		             			result.put(1,1);
		             		}
		             	// 2 terms
		             	}else if(count == 2){
		             		if(result.get(2)!= null){
		             			int v = result.get(2);
			             		result.put(2, v+1);
		             		}else{
		             			result.put(2,1);
		             		}
		             	// 3 terms
		             	}else if(count == 3){
		             		if(result.get(3)!= null){
		             			int v = result.get(3);
			             		result.put(3, v+1);
		             		}else{
		             			result.put(3,1);
		             		}
		             	// more than 3 terms
		             	}else if(count > 3){
		             		if(result.get(4)!= null){
		             			int v = result.get(4);
			             		result.put(4, v+1);
		             		}else{
		             			result.put(4,1);
		             		}
		             	}
		             	
		             	tokenizedQuery.clear();
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }         
         
         System.out.println("end");
		 return result; 
	 }

	 public static int maxOperators() throws IOException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         int result = 0;
         ArrayList<String> tokenizedQuery = new ArrayList<String>();         
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	if(query.equals("-")){
		             		tokenized.clear();
		             		continue;
		             	}
		             	int count = 0;
		                Pattern p = Pattern.compile("and");
		                Matcher m = p.matcher(query);
		                while (m.find()){
		                	count +=1;
		                }
		                p = Pattern.compile("or");
		                m = p.matcher(query);
		                while (m.find()){
		                	count +=1;
		                }
		                p = Pattern.compile("not");
		                m = p.matcher(query);
		                while (m.find()){
		                	count +=1;
		                }
		                p = Pattern.compile("near");
		                m = p.matcher(query);
		                while (m.find()){
		                	count +=1;
		                }
		            	count += query.length() - query.replace("-", "").length();
		            	count += query.length() - query.replace("+", "").length();
		            	if(count > result){
		            		result = count;
		            	}
		             	tokenizedQuery.clear();
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }         
         
         System.out.println("end");
		 return result; 
	 }

	 public static float totalOperators() throws IOException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         int count = 0;
         ArrayList<String> tokenizedQuery = new ArrayList<String>();         
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	// tokenize line	
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	if(query.equals("-")){
		             		tokenized.clear();
		             		continue;
		             	}
		                Pattern p = Pattern.compile("and");
		                Matcher m = p.matcher(query);
		                while (m.find()){
		                	count +=1;
		                }
		                p = Pattern.compile("or");
		                m = p.matcher(query);
		                while (m.find()){
		                	count +=1;
		                }
		                p = Pattern.compile("not");
		                m = p.matcher(query);
		                while (m.find()){
		                	count +=1;
		                }
		                p = Pattern.compile("near");
		                m = p.matcher(query);
		                while (m.find()){
		                	count +=1;
		                }
		            	count += query.length() - query.replace("-", "").length();
		            	count += query.length() - query.replace("+", "").length();
		            	
		             	tokenizedQuery.clear();
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }         
         
         System.out.println("end");
		 return count; 
	 }

	 public static ArrayList<IR_Tuple> mostPopularQueries() throws IOException{
		 HashMap<String, Integer> queries = new HashMap<>();
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	// tokenize line
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	if(query.equals("-")){
		             		tokenized.clear();
		             		continue;
		             	}
		             	// populate hashmap
		             	int v;		             			
	             		if(queries.containsKey(query)){
	             			v = queries.get(query);
	             			queries.put(query, v+1);
	             			
	             		}else{
	             			v = 1;
		             		queries.put(query, v);
	             		}
		             	
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }
         // Sort hashmap
         ArrayList<IR_Tuple>sorted = new ArrayList<IR_Tuple>();
         //iterate through map and put key-values to the arraylist
		for (Entry<String, Integer> entry : queries.entrySet())
		{
		    //System.out.println(entry.getKey() + "/" + entry.getValue());
		    IR_Tuple a = new IR_Tuple(entry.getKey(), entry.getValue());
		    sorted.add(a);
		}
		 //sort arraylist
		Comparator<IR_Tuple> comparator = new Comparator<IR_Tuple>(){

					@Override
					public int compare(IR_Tuple o1, IR_Tuple o2) {
						if(o1.getValue() < o2.getValue()){
							return 1;
						}
						else if(o1.getValue() > o2.getValue()){
							return -1;
						}
						return 0;
					}

		};
		Collections.sort(sorted,comparator);
         //return top 25 to result.
         
         for(int i=0 ; i< 25; ++i) {
             System.out.println(sorted.get(i).getTerm() +" & "+ sorted.get(i).getValue());
         }
         System.out.println("end");
		 return sorted;
	 }

	 public static ArrayList<IR_Tuple> mostCommonNonStopWords() throws IOException{
		 HashMap<String, Integer> words = new HashMap<>();
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         StringTokenizer qt;
         // populate stopwords
         HashMap<String, Integer> stopWords = new HashMap<>();
         reader = new BufferedReader(new FileReader(filePath 
		   		 + "/src/stopwords.txt"));
         while((line = reader.readLine()) != null){
        	 st = new StringTokenizer(line);
        	 String w = st.nextToken();
        	 stopWords.put(w,1);
         }
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try{       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	// tokenize line
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	
		             	// populate hashmap
		             	if(!query.contains("download")){
		             		tokenized.clear();
		             		continue;
		             	}
		             	int v;		  
		             	qt = new StringTokenizer(query, " ");
		             	// populate hashmap of words
		             	while(qt.hasMoreTokens()){
		             		String token = qt.nextToken();
		             		if(token.contains("download")){
		             			continue;
		             		}
		             		if(words.containsKey(token)){
		             			v = words.get(token);
		             			// if word doesnt exist in stopwords list
		             			if(stopWords.get(token) == null){
			             			words.put(token, v+1);
		             			}
		             		}else{
		             			// if word doesnt exist in stopwords list	
		             			if(stopWords.get(token) == null){
		             				v = 1;
			             			words.put(token, v);
		             			}
		             			
		             		}
		             		if(words.containsKey(query)){
		             			v = words.get(query);
		             			words.put(query, v+1);
		             			
		             		}else{
		             			v = 1;
		             			words.put(query, v);
		             		}
			             	
		             	}
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }
         // Sort hashmap
        ArrayList<IR_Tuple> sorted = new ArrayList<IR_Tuple>();
         //iterate through map and put key-values to the arraylist
		for (Entry<String, Integer> entry : words.entrySet())
		{
		    //System.out.println(entry.getKey() + "/" + entry.getValue());
		    IR_Tuple a = new IR_Tuple(entry.getKey(), entry.getValue());
		    sorted.add(a);
		}
		 //sort arraylist
		Comparator<IR_Tuple> comparator = new Comparator<IR_Tuple>(){

					@Override
					public int compare(IR_Tuple o1, IR_Tuple o2) {
						if(o1.getValue() < o2.getValue()){
							return 1;
						}
						else if(o1.getValue() > o2.getValue()){
							return -1;
						}
						return 0;
					}

		};
		Collections.sort(sorted,comparator);
         System.out.println("end");
         //return result sorted
		 return sorted;
	 }

	 public static HashMap<Integer, Integer> queriesPerSession() throws IOException, ParseException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         HashMap<Integer, Integer> result = new HashMap<Integer,Integer>();
         ArrayList<String> tokenizedQuery = new ArrayList<String>();  
         int previousID = 0;
         String previousQuery = null;
		 Date previousTime = null;
		 boolean firstLine = true;
		 int queriesPerSession = 0;
		 
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	//tokenize first line
		            	if(firstLine==true){
		            		st = new StringTokenizer(line, "\t");
			                while(st.hasMoreTokens()){
			                	tokenized.add(st.nextToken());
			                }
			                
			             	previousID = Integer.parseInt(tokenized.get(0));
			             	previousQuery = tokenized.get(1);
			             	previousTime = DateFormat.parse(tokenized.get(2));
			             	firstLine=false;
			             	tokenized.clear();
			             	continue;
		            	}		            	
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	// skip empty queries
		             	if(query.equals("-")){
		             		tokenized.clear();
		             		continue;
		             	}
		             	// find sessions
		             	//difference in seconds
		             	long dateDiff = queryTime.getTime()-previousTime.getTime();
		             	long diffSeconds = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
		             	// count queries per session
		             	if(anonID!=previousID || (diffSeconds >= 300 && anonID == previousID)){
		             		queriesPerSession = 0;
		             	}
		             	else{
		             		queriesPerSession ++;
		             	}
		             	// 1 query per session
		             	if(queriesPerSession == 1){
		             		if(result.get(1) == null){
		             			result.put(1, 1);
		             		}
		             		else{
		             			int v = result.get(1);
		             			result.put(1, v+1);
		             		}
		             	}
		                // 2 query per session
		             	if(queriesPerSession == 2){
		             		if(result.get(2) == null){
		             			result.put(2, 1);
		             		}
		             		else{
		             			int v = result.get(2);
		             			result.put(2, v+1);
		             		}
		             	}
		                // 3 query per session
		             	if(queriesPerSession == 3){
		             		if(result.get(3) == null){
		             			result.put(3, 1);
		             		}
		             		else{
		             			int v = result.get(3);
		             			result.put(3, v+1);
		             		}
		             	}
		               // >3 query per session
		             	if(queriesPerSession > 3){
		             		if(result.get(4) == null){
		             			result.put(4, 1);
		             		}
		             		else{
		             			int v = result.get(4);
		             			result.put(4, v+1);
		             		}
		             	}
		             	//set current variables to previous, to prepare for next line
		             	previousID=anonID;
		             	previousTime=queryTime;
		             	tokenizedQuery.clear();
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }         
         
         System.out.println("end");
		 return result; 
	 }

	 public static int maxQueriesPerSession() throws IOException, ParseException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         int result = 0;
         ArrayList<String> tokenizedQuery = new ArrayList<String>();  
         int previousID = 0;
         String previousQuery = null;
		 Date previousTime = null;
		 boolean firstLine = true;
		 int queriesPerSession = 0;
		 
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	//tokenize first line
		            	if(firstLine==true){
		            		st = new StringTokenizer(line, "\t");
			                while(st.hasMoreTokens()){
			                	tokenized.add(st.nextToken());
			                }
			                
			             	previousID = Integer.parseInt(tokenized.get(0));
			             	previousQuery = tokenized.get(1);
			             	previousTime = DateFormat.parse(tokenized.get(2));
			             	firstLine=false;
			             	tokenized.clear();
			             	continue;
		            	}		            	
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	// skip empty queries
		             	if(query.equals("-")){
		             		tokenized.clear();
		             		continue;
		             	}
		             	// find sessions
		             	//difference in seconds
		             	long dateDiff = queryTime.getTime()-previousTime.getTime();
		             	long diffSeconds = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
		             	// count queries per session
		             	if(anonID!=previousID || (diffSeconds >= 300 && anonID == previousID)){
		             		if(queriesPerSession > result){
		             			result = queriesPerSession;
		             		}
		             		queriesPerSession = 0;
		             	}
		             	else{
		             		queriesPerSession ++;
		             	}
		             	
		             	//set current variables to previous, to prepare for next line
		             	previousID=anonID;
		             	previousTime=queryTime;
		             	tokenizedQuery.clear();
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }         
         
         System.out.println("end");
		 return result; 
	 }
	
	 public static int percentageOfQueriesAskedByOneUsers() throws IOException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         HashMap<String, Integer> queriesPerUser = new HashMap<String, Integer>();
         
         ArrayList<String> tokenizedQuery = new ArrayList<String>();         
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	
		            	
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	// skip if line empty
		             	if(query.equals("-")){
		             		tokenized.clear();
		             		continue;
		             	}
		             	if(!queriesPerUser.containsKey(query)){
		             		queriesPerUser.put(query, anonID);
		             	}else{
		             		if(queriesPerUser.get(query)!=-1){
		             			queriesPerUser.put(query, -1);
		             			
		             		}
		             	}
		             	tokenizedQuery.clear();
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }         
         ArrayList<String> sorted = new ArrayList<String>();
         //iterate through map and put keys to the arraylist
		 for (Entry<String, Integer> entry : queriesPerUser.entrySet())
		 {
			 if(entry.getValue()!=-1){
				sorted.add(entry.getKey());
			 }
		 }
         System.out.println("end");
		 return sorted.size(); 
	 }

	 public static int deletedTermsInQueries() throws IOException, ParseException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         StringTokenizer qt;
         int result = 0;
         ArrayList<String> tokenizedQuery = new ArrayList<String>();  
         int previousID = 0;
         String previousQuery = null;
		 Date previousTime = null;
		 boolean firstLine = true;
		 
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {

		            	//tokenize first line
		            	if(firstLine==true){
		            		st = new StringTokenizer(line, "\t");
			                while(st.hasMoreTokens()){
			                	tokenized.add(st.nextToken());
			                }
			                
			             	previousID = Integer.parseInt(tokenized.get(0));
			             	previousQuery = tokenized.get(1);
			             	previousTime = DateFormat.parse(tokenized.get(2));
			             	firstLine=false;
			             	tokenized.clear();
			             	continue;
		            	}
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	// skip empty queries
		             	if(query.equals("-")){
		             		tokenized.clear();
		             		continue;
		             	}
		             	// find sessions
		             	//difference in seconds
		             	long dateDiff = queryTime.getTime()-previousTime.getTime();
		             	long diffSeconds = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
		             	// check if added terms per session
		             	if(anonID!=previousID || (diffSeconds >= 300 && anonID == previousID)){
		             		// new session
		             		// do nothing
		             	}
		             	else{
		             		qt = new StringTokenizer(query);
		             		while(qt.hasMoreTokens()){
		             			tokenizedQuery.add(qt.nextToken());
		             		}
		             		// same session
		             		for(String s : tokenizedQuery){
		             			if(previousQuery.contains(s) && (previousQuery.length() > query.length())){
		             				result++;
		             				break;
		             			}
		             		}
		             	}
		             	
		             	//set current variables to previous, to prepare for next line
		             	previousID=anonID;
		             	previousTime=queryTime;
		             	tokenizedQuery.clear();
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }         
         
         System.out.println("end");
		 return result; 
	 }

	 public static int addedTermsInQueries() throws IOException, ParseException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         StringTokenizer qt;
         int result = 0;
         ArrayList<String> tokenizedQuery = new ArrayList<String>();  
         int previousID = 0;
         String previousQuery = null;
		 Date previousTime = null;
		 boolean firstLine = true;
		 
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {

		            	//tokenize first line
		            	if(firstLine==true){
		            		st = new StringTokenizer(line, "\t");
			                while(st.hasMoreTokens()){
			                	tokenized.add(st.nextToken());
			                }
			                
			             	previousID = Integer.parseInt(tokenized.get(0));
			             	previousQuery = tokenized.get(1);
			             	previousTime = DateFormat.parse(tokenized.get(2));
			             	firstLine=false;
			             	tokenized.clear();
			             	continue;
		            	}
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	// skip empty queries
		             	if(query.equals("-")){
		             		tokenized.clear();
		             		continue;
		             	}
		             	// find sessions
		             	//difference in seconds
		             	long dateDiff = queryTime.getTime()-previousTime.getTime();
		             	long diffSeconds = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
		             	// check if added terms per session
		             	if(anonID!=previousID || (diffSeconds >= 300 && anonID == previousID)){
		             		// new session
		             		// do nothing
		             	}
		             	else{
		             		qt = new StringTokenizer(query);
		             		while(qt.hasMoreTokens()){
		             			tokenizedQuery.add(qt.nextToken());
		             		}
		             		// same session
		             		for(String s : tokenizedQuery){
		             			if(previousQuery.contains(s) && (previousQuery.length() < query.length())){
		             				result++;
		             				break;
		             			}
		             		}
		             	}
		             	
		             	//set current variables to previous, to prepare for next line
		             	previousID=anonID;
		             	previousTime=queryTime;
		             	tokenizedQuery.clear();
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }         
         
         System.out.println("end");
		 return result; 
	 }

	 public static int totalChangeInQueries() throws IOException, ParseException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         StringTokenizer qt;
         int result = 0;
         ArrayList<String> tokenizedQuery = new ArrayList<String>();  
         int previousID = 0;
         String previousQuery = null;
		 Date previousTime = null;
		 boolean firstLine = true;
		 boolean foundTerm = false;
		 
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {

		            	//tokenize first line
		            	if(firstLine==true){
		            		st = new StringTokenizer(line, "\t");
			                while(st.hasMoreTokens()){
			                	tokenized.add(st.nextToken());
			                }
			                
			             	previousID = Integer.parseInt(tokenized.get(0));
			             	previousQuery = tokenized.get(1);
			             	previousTime = DateFormat.parse(tokenized.get(2));
			             	firstLine=false;
			             	tokenized.clear();
			             	continue;
		            	}
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	// skip empty queries
		             	if(query.equals("-")){
		             		tokenized.clear();
		             		continue;
		             	}
		             	// find sessions
		             	//difference in seconds
		             	long dateDiff = queryTime.getTime()-previousTime.getTime();
		             	long diffSeconds = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
		             	// check if added terms per session
		             	if(anonID!=previousID || (diffSeconds >= 300 && anonID == previousID)){
		             		// new session
		             		// do nothing
		             	}
		             	else{
		             		qt = new StringTokenizer(query);
		             		while(qt.hasMoreTokens()){
		             			tokenizedQuery.add(qt.nextToken());
		             		}
		             		// same session
		             		for(String currentTerm : tokenizedQuery){
		             			// if the term exists in the previous query
		             			// then the query is not completely changed
		             			if(previousQuery.contains(currentTerm)){
		             				foundTerm = true;
		             				break;
		             			}else{
		             				foundTerm = false;
		             			}
		             		}
		             		if(foundTerm == false){
			             		result++;
		             		}
		             	}
		             	
		             	//set current variables to previous, to prepare for next line
		             	previousID=anonID;
		             	previousTime=queryTime;
		             	tokenizedQuery.clear();
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }         
         
         System.out.println("end");
		 return result; 
	 }

	 public static HashMap<Integer, Integer> distrinctQueriesAskedFrequency() throws IOException{
		 HashMap<String, Integer> queries = new HashMap<>();
		 HashMap<Integer, Integer> result = new HashMap<Integer,Integer>();
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try
		        {       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	// tokenize line
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	if(query.equals("-")){
		             		tokenized.clear();
		             		continue;
		             	}
		             	// populate queries hashmap
		             	int v;		             			
	             		if(queries.containsKey(query)){
	             			v = queries.get(query);
	             			queries.put(query, v+1);
	             			
	             		}else{
	             			v = 1;
		             		queries.put(query, v);
	             		}
		             	
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }
         //iterate through map and put key-values to the result hashmap
		for (Entry<String, Integer> entry : queries.entrySet())
		{
			if(entry.getValue()==0){
				if(result.containsKey(0)){
					int v = result.get(0);
					result.put(0, v+1);
				}else{
					result.put(0, 1);
				}
			}if(entry.getValue()==1){
				if(result.containsKey(1)){
					int v = result.get(1);
					result.put(1, v+1);
				}else{
					result.put(1, 1);
				}
			}if(entry.getValue()==2){
				if(result.containsKey(2)){
					int v = result.get(2);
					result.put(2, v+1);
				}else{
					result.put(2, 1);
				}
			}if(entry.getValue()==3){
				if(result.containsKey(3)){
					int v = result.get(3);
					result.put(3, v+1);
				}else{
					result.put(3, 1);
				}
			}if(entry.getValue()>3){
				if(result.containsKey(4)){
					int v = result.get(4);
					result.put(4, v+1);
				}else{
					result.put(4, 1);
				}
			}
		    
		    
		}
		
         for(int i=0 ; i< 5; ++i) {
             System.out.println(i + " & "+ result.get(i));
         }
         
         System.out.println("end");
		 return result;
	 }

	 public static HashMap<String, Integer> mostCommon5000Terms() throws IOException{
		 HashMap<String, Integer> words = new HashMap<>();
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         StringTokenizer qt;
         // populate stopwords
         HashMap<String, Integer> stopWords = new HashMap<>();
         reader = new BufferedReader(new FileReader(filePath 
		   		 + "/stopwords.txt"));
         while((line = reader.readLine()) != null){
        	 st = new StringTokenizer(line);
        	 String w = st.nextToken();
        	 stopWords.put(w,1);
         }
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try{       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	// tokenize line
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	if(query.equals("-")){
		             		tokenized.clear();
		             		continue;
		             	}
		             	// populate hashmap
		             	int v;		  
		             	qt = new StringTokenizer(query, " ");
		             	// populate hashmap of words
		             	while(qt.hasMoreTokens()){
		             		String token = qt.nextToken();
		             		if(words.containsKey(token)){
		             			v = words.get(token);
		             			// if word doesnt exist in stopwords list
		             			if(stopWords.get(token) == null){
			             			words.put(token, v+1);
		             			}
		             		}else{
		             			// if word doesnt exist in stopwords list	
		             			if(stopWords.get(token) == null){
		             				v = 1;
			             			words.put(token, v);
		             			}
		             			
		             		}
			             	
		             	}
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }
         // Sort hashmap
        ArrayList<IR_Tuple> sorted = new ArrayList<IR_Tuple>();
         //iterate through map and put key-values to the arraylist
		for (Entry<String, Integer> entry : words.entrySet())
		{
		    //System.out.println(entry.getKey() + "/" + entry.getValue());
		    IR_Tuple a = new IR_Tuple(entry.getKey(), entry.getValue());
		    sorted.add(a);
		}
		 //sort arraylist
		Comparator<IR_Tuple> comparator = new Comparator<IR_Tuple>(){

					@Override
					public int compare(IR_Tuple o1, IR_Tuple o2) {
						if(o1.getValue() < o2.getValue()){
							return 1;
						}
						else if(o1.getValue() > o2.getValue()){
							return -1;
						}
						return 0;
					}

		};
		Collections.sort(sorted,comparator);
         System.out.println("end");
         HashMap<String, Integer> result = new HashMap<String, Integer>();
         for(int i=0; i<5000; i++){
        	 result.put(sorted.get(i).getTerm(),1);
         }
		 return result;
	 }

	 public static HashMap<IR_Record, Boolean> filteredLog(HashMap<String, Integer> wordList) throws IOException, ParseException{
		 String line = null;
		 BufferedReader reader = null;
         ArrayList<String> tokenized = new ArrayList<String>();
         StringTokenizer st;
         StringTokenizer qt;
         HashMap<IR_Record, Boolean> result = new HashMap<IR_Record, Boolean>();
         for(int j=1;j<=10;j++){
        	 String identifier = String.valueOf(j);
			try{       
				if(j<10){
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/DBTextFiles/user-ct-test-collection-0"+identifier+".txt"));
					/*reader = new BufferedReader(new FileReader(filePath 
					   		 + "/src/DBTextFiles/ses.txt"));*/
				}
				else{
					reader = new BufferedReader(new FileReader(filePath 
					   		 + "/DBTextFiles/user-ct-test-collection-10.txt"));
				}
		            while ((line = reader.readLine()) != null)
		            {
		            	// tokenize line
		            	st = new StringTokenizer(line, "\t");
		                while(st.hasMoreTokens()){
		                	tokenized.add(st.nextToken());
		                }
		             	anonID = Integer.parseInt(tokenized.get(0));
		             	query = tokenized.get(1);
		             	queryTime = DateFormat.parse(tokenized.get(2));
		             	if(tokenized.size()>3){
		             		itemRank = Integer.parseInt(tokenized.get(3));
		             		clickURL = tokenized.get(4);
		             	}
		             	if(query.equals("-")){
		             		tokenized.clear();
		             		continue;
		             	}
		             	// populate hashmap
		             	qt = new StringTokenizer(query, " ");
		             	// populate hashmap of words
		             	while(qt.hasMoreTokens()){
		             		String token = qt.nextToken();
		             		// if we find a record that contains one of the 5000 words
		             		// and if we haven't added already the query to the filtered log
		             		// we add the new tuple
		             		if(wordList.containsKey(token)){
		             			IR_Record temp = new IR_Record(anonID, query, queryTime, itemRank, clickURL);
		             			if(!result.containsKey(temp)){
				             			result.put(temp, true);
		             			}
		             			
		             		}
			             	
		             	}
		             	tokenized.clear();
		            }
		        }
		        catch (IOException ex)
		        {
		            ex.printStackTrace();
		        }               
	
		        finally
		        {
		            reader.close();
		        }
			System.out.println("Progress:" + j+"0%");
         }
         
         System.out.println("end");
		 return result;
	 }
} 




