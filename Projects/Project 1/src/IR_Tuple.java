import java.util.Comparator;


public class IR_Tuple{
		 private String term;
		 private Integer value;
		
		 public IR_Tuple(String term, Integer value){
			 this.term = term;
			 this.value = value;
		 }
		 
		 public String getTerm(){
			 return term;
		 }
		 
		 public Integer getValue(){
			 return value;
		 }
		 
}
