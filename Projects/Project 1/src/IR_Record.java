import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class IR_Record {
	private int anonID;
	private String query;
	private Date queryTime;
	private int itemRank;
	private String clickURL;
	
	public IR_Record(int anonID, String query, Date queryTime, int itemRank, String clickURL){
		this.anonID = anonID;
		this.query = query;
		this.queryTime = queryTime;
		this.itemRank = itemRank;
		this.clickURL = clickURL;
		
	}
	
	public int getAnonID(){
		return anonID;
	}
	
	public String getQuery(){
		return query;
	}
	
	public Date getQueryTime(){
		return queryTime;
	}
	
	public int getItemRank(){
		return itemRank;
	}
	
	public String getClickURL(){
		return clickURL;
	}
}
