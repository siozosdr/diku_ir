# README #

This repo contains the Information Retrieval assigned projects in the MSc in Computer Science program of DIKU.
Project implementations can be found inside each Project's "src" folder, while the final report is on the base folder
of the project with the name "template-IR-project*.pdf".

## Project 1 ##

In this project an analysis of an AOL Search En-
gine query log is presented, consisting of approximately 20 million entries
for search requests, made by about 650 thousand users, over
a period of three months.

## Project 2 ##

The aim of this project is to see whether and to what
extent the relevance feedback method of He and Ounis im-
proves retrieval eectiveness over retrieval without relevance
feedback (baseline).